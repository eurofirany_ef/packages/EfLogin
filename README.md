# EfLogin

Login to application via AC data.

## Install
```bash
composer require eurofirany/ef-login
```

##### Publish config
```bash
php artisan vendor:publish --tag=ef_ac_connector
```

```bash
php artisan vendor:publish --tag=ef_login
```

##### Create table for auth logs
```bash
php artisan migrate
```

## Configuration

```php
<?php

return [
    'token' => env('EF_API_TOKEN'),
    'permissions' => [
        // Groups
        0123 => [ 
            // Route name
            'redirect' => 'home',

            // AC Field mapping (saved in session after sucessful login)
            'map' => [
                'login' => 'kod_operatora',
                'name' => 'imienazwisko'
            ],
            // Custom variables (saved in session after sucessful login)
            'variables' => [
                'test' => true
            ]
        ],
    ]
];
```

## Variables for .env

```bash
# EfAcConnector
AC_LOGIN=
AC_WS1=
AC_WS_PASSWORD=
AC_EFWebService=
AC_EFWebService_PASSWORD=


# EfLogin
EF_TOKEN=
```

## Defined routes
```php
// Web
login 
logout

// Api
ef-login/permissions
ef-login/logs
```

## Usage (middleware)

```php
Route::middleware(['efAuth'])->group(function () {
    //
});
```


### Dependencies 
> - [EfAcConnector](https://bitbucket.org/eurofirany-paczki/efacconnector/src/master)
