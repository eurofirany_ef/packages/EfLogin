<?php

return [
    'token' => env('EF_TOKEN'),
    'permissions' => [
        // Groups
        1162 => [
            // Route name
            'redirect' => 'home',
            // AC Field mapping
            'map' => [
                'login' => 'kod_operatora',
                'name' => 'imienazwisko'
            ],
            // Custom variables
            'variables' => [
                'isAdmin' => true
            ]
        ],
    ]
];
