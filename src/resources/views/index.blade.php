<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>{{ config('app.name') }} - Formularz logowania</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('vendor/ef-login/style.css') }}">
</head>

<body>
<div id="app">
    <section class="section">
        <div class="container mt-5">
            <div class="row">
                <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                    <div class="login-brand">
                        <img src="{{ asset('vendor/ef-login/img/logo.png') }}" alt="logo" width="200"
                             class="shadow-light rounded-circle mx-auto d-block">
                    </div>

                    <div class="card card-primary mt-5">
                        <div class="card-header">
                            <h4>{{ config('app.name') }}</h4>
                            <h6 class="text-muted">Formularz logowania</h6>
                        </div>

                        <div class="card-body">
                            <form method="POST" action="#" class="needs-validation" novalidate="">
                                @csrf
                                <div class="form-group">
                                    <label for="login">Login AC</label>
                                    <input id="login" type="login" class="form-control" name="login" tabindex="1"
                                           required autofocus value="{{ old('login') }}">
                                    <div class="invalid-feedback">
                                        Uzupełnij pole "login"
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="d-block">
                                        <label for="password" class="control-label">Hasło</label>
                                    </div>
                                    <input id="password" type="password" class="form-control" name="password"
                                           tabindex="2" required value="{{ old('password') }}">
                                    <div class="invalid-feedback">
                                        Uzupełnij pole "hasło"
                                    </div>
                                </div>

                                @if(session('error'))
                                    <div class="alert alert-danger p-1">
                                        <span>{{ session('error') }}</span>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                                        Zaloguj
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="simple-footer mt-2 pt-2 text-muted text-center">
                        &copy; {{ date('Y') }} Eurofirany B.B. Choczyńscy Sp.J.
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</body>
</html>
