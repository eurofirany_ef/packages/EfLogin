<?php

namespace Eurofirany\EfLogin\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Eurofirany\EfLogin\Repositories\EfAuthLogRepository;

class EfAuthLogController extends Controller
{
    private $efAuthLogRepository;

    public function __construct(EfAuthLogRepository $efAuthLogRepository)
    {
        $this->efAuthLogRepository = $efAuthLogRepository;
    }

    public function index(Request $request)
    {
        if ($request->has('token') && $request->token === config('ef_login.token'))
            return response()->json([
                $this->efAuthLogRepository->getAuthLogs()
            ], 200);
        else
            abort(401);
    }
}
