<?php 

namespace Eurofirany\EfLogin\Http\Controllers;

use App\Http\Controllers\Controller;
use Eurofirany\EfLogin\EfLogin;
use Eurofirany\EfLogin\Http\Requests\LoginRequest;

class EfLoginController extends Controller {
    
    public function index() 
    {
        return view('ef-login::index');
    }

    public function login(LoginRequest $request, EfLogin $efLogin) 
    {
        $loginWorker = $efLogin->loginWorker([
            'login' => $request->login,
            'password' => $request->password
        ]);
        
        $userData = [
            'user' => $request->login,
            'ip_address' => $request->ip(),
            'device' => $request->userAgent()
        ];

        if (!$loginWorker['status']) {
            $efLogin->createFailedAuthLog($userData, $loginWorker['opis']);

            session()->flash('error', $loginWorker['opis']);

            return back();
        }

        if (isset($loginWorker['grupy_uprawnien'])) {
            $auth = $efLogin->auth($loginWorker);
            
            if($auth) {

                $efLogin->createSuccessfulAuthLog($userData);

                return redirect()->route($efLogin->getRedirect());
            }
        }

        $efLogin->createFailedAuthLog($userData, 'Nieprawidłowa grupa uprawnień');

        session()->flash('error', 'Nieprawidłowa grupa uprawnień');

        return back();
    }

    public function logout(EfLogin $efLogin) 
    {
        $efLogin->updateAuthLog();

        session()->flush();
  
        return redirect('login');
    }
}