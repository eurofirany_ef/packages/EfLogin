<?php

namespace Eurofirany\EfLogin\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Eurofirany\EfLogin\EfLogin;

class EfAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->session()->has('efAuth') == true) {
            app(EfLogin::class)->updateAuthLog(session()->get('sessionId'));
            return $next($request);
        }

        return redirect()->route('login');
    }
}
