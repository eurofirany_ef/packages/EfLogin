<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEfAuthLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ef_auth_logs', function (Blueprint $table) {
            $table->id();
            $table->string('session', 40)->nullable();
            $table->string('user', 100);
            $table->ipAddress('ip_address', 39);
            $table->longText('device');
            $table->dateTime('logged_in_at')->nullable();
            $table->dateTime('logged_out_at')->nullable();
            $table->dateTime('failed_login_at')->nullable();
            $table->text('failed_login_details')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ef_auth_logs');
    }
}
