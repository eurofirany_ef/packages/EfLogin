<?php

namespace Eurofirany\EfLogin\Models;

use Illuminate\Database\Eloquent\Model;

class EfAuthLog extends Model
{
    public $timestamps = false;

    protected $hidden = ['session'];

    protected $fillable = [
        'session',
        'user',
        'ip_address',
        'device',
        'logged_in_at',
        'logged_out_at',
        'failed_login_at',
        'failed_login_details'
    ];
}
