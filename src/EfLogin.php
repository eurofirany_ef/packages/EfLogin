<?php

namespace Eurofirany\EfLogin;

use Eurofirany\EfAcConnector\EfAcConnector;
use Eurofirany\EfLogin\Repositories\EfAuthLogRepository;

class EfLogin
{
    private string $redirect;
    private EfAuthLogRepository $efAuthLogRepository;

    public function __construct(EfAuthLogRepository $efAuthLogRepository)
    {
        $this->efAuthLogRepository = $efAuthLogRepository;
    }

    public function loginWorker(array $params): array
    {
        $efAcConnector = new EfAcConnector();

        $efAcConnector->setParameters([
            'Login' => $params['login'],
            'MD5Pass' => md5($params['password'])
        ]);

        $response = $efAcConnector->request('LoginPrac', 'WS1');

        // Return parsed collection
        return (array)$response->LoginPracResult ?? [];
    }

    public function auth(array $login): bool
    {
        foreach (explode(',', $login['grupy_uprawnien']) as $permission) {
            $definedPermissions = config('ef_login.permissions');

            if (isset($definedPermissions[$permission])) {
                foreach ($definedPermissions[$permission]['map'] as $key => $value) {
                    if (isset($login[$value]))
                        session([$key => $login[$value]]);
                }

                foreach ($definedPermissions[$permission]['variables'] as $key => $value) {
                    session([$key => $value]);
                }

                // Start session
                $this->generateSession();

                // Auth middleware
                $this->setRedirect($definedPermissions[$permission]['redirect']);

                return true;
            }
        }

        return false;
    }


    private function generateSession(): void
    {
        session([
            'efAuth' => true,
            'sessionId' => sha1(time())
        ]);
    }

    public function createFailedAuthLog(array $authLog, string $details): object
    {
        $authLog['failed_login_details'] = $details;
        $authLog['failed_login_at'] = now();

        return $this->efAuthLogRepository->createAuthLog($authLog);
    }

    public function createSuccessfulAuthLog(array $authLog): object
    {
        $authLog['session'] = session()->get('sessionId');
        $authLog['logged_in_at'] = now();

        return $this->efAuthLogRepository->createAuthLog($authLog);
    }

    public function updateAuthLog(): int
    {
        $sessionId = session()->get('sessionId');

        return $this->efAuthLogRepository->updateAuthLog($sessionId);
    }

    private function setRedirect(string $redirect): string
    {
        return $this->redirect = $redirect;
    }

    public function getRedirect(): string
    {
        return $this->redirect;
    }

}
