<?php

namespace Eurofirany\EfLogin;

use Illuminate\Support\ServiceProvider;
use Eurofirany\EfLogin\Http\Middleware\EfAuthMiddleware;

class EfLoginServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'ef-login');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path('ef_login.php'),
            ], 'ef_login');

            // Publishing the views.
            $this->publishes([
                __DIR__ . '/resources/views' => resource_path('views/vendor/ef-login'),
            ], 'ef_login');

            // Publishing assets.
            $this->publishes([
                __DIR__ . '/resources/assets' => public_path('vendor/ef-login'),
            ], 'ef_login');
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'ef_login');

        // Middleware
        app('router')->aliasMiddleware('efAuth', EfAuthMiddleware::class);
    }
}
