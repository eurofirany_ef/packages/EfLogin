<?php

namespace Eurofirany\EfLogin\Repositories;

use Eurofirany\EfLogin\Models\EfAuthLog;

class EfAuthLogRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EfAuthLog::class;
    }

    public function getAuthLogs()
    {
        return EfAuthLog::whereDate('logged_in_at', '>=', date('Y-m-d', strtotime('-7 day')))
            ->get();
    }

    public function createAuthLog(array $authLog)
    {
        return EfAuthLog::create($authLog);
    }

    public function updateAuthLog(string $sessionId)
    {
        return EfAuthLog::where('session', $sessionId)
            ->update(['logged_out_at' => now()]);
    }
}
