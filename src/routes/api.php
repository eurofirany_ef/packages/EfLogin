<?php

use Illuminate\Support\Facades\Route;
use Eurofirany\EfLogin\Http\Controllers\EfAuthLogController;

Route::group(['prefix' => 'ef-login', 'middleware' => 'ef-login-middleware'], function () {
    Route::get('/permissions', function () {
        return response()->json([
            array_keys(config('ef_login.permissions'))
        ], 200);
    });
    Route::get('/logs', [EfAuthLogController::class, 'index']);
});
