<?php

use Eurofirany\EfLogin\Http\Controllers\EfLoginController;
use Illuminate\Support\Facades\Route;


Route::group(['middleware' => ['web']], function () {
    Route::get('/login', [EfLoginController::class, 'index'])->name('login');
    Route::post('/login', [EfLoginController::class, 'login']);
    Route::get('/logout', [EfLoginController::class, 'logout'])->name('logout');
});